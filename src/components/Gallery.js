import React, { Component } from "react";
import { Carousel } from 'react-responsive-carousel';

const Gallery = (props) => {
    return (
      props.images.length &&
      <Carousel className="gallery" showArrows={false} showStatus={false} showThumbs={true}>
        {props.images.map( (image,i) => {
            return (<div key={i}><img src={image.src1130}></img></div>)
        })}
      </Carousel>
    );
}

module.exports = Gallery;
