import React, { Component } from "react";

const ColorSelector = (props) => {
  return ( 
    <div className="color-selector">
      <div className="color-selector__selection-label">
        COLOR:
        <span>{props.selectedColor}</span>
      </div>
      <div className="color-selector__palets-container">
        {props.colors.map((color, i) => {
          return ( 
            <div key={i} 
              className={`color-selector__color color-selector__color--${color} ${color === props.selectedColor ? 'selected': ''}`} 
              onClick={props.handleColorSelected.bind(this, color)}> 
            </div>
          );
        })}
      </div>
    </div> 
  );
}

module.exports = ColorSelector;

