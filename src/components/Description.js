import React, { Component } from "react";

const Description = (props) => {
  return ( 
    <div className="description">
      <h2 className="description__title">DETAILS</h2>
      <div className="description__body" dangerouslySetInnerHTML={{__html: props.description }}></div>
    </div> 
  );
}

module.exports = Description;
