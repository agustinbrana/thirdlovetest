import React, { Component } from "react";

const OptionGroup = (props) => {
    const selectedOption = props.selectedOption;
    const options = props.options.map( (option,i) => {
        if (selectedOption === option) {
            return <option key={i} selected value={option}> {option} </option>;
        } else {
            return <option key={i} value={option}> {option} </option>;
        }
    });

    function onChange(event) {
        props.handleOptionSelected(event.target.selectedIndex - 1);
    }

    return ( 
        <div className="option-group">
            <div className="option-group__label">{props.label}</div>
            <div className = "option-group__select">
                <div className = "option-group__value"> {selectedOption} </div> 
                <select onChange = {onChange.bind(this)}> 
                    <option>Choose one</option>;
                    {options} 
                </select> 
            </div>
        </div>
    );
}

module.exports = OptionGroup;