import React, { Component } from "react";

const Heading = (props) => {
  return ( 
    <div className="heading">
      <div className="heading__column"></div>
      <div className="heading__wrapper">
        <h1 className="heading__title">{props.title}</h1>
        <span className="heading__price">${Math.round(props.price)}</span>
      </div>
    </div> 
  );
}

module.exports = Heading;
