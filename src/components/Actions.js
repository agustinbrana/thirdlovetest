import React, { Component } from "react";
import ColorSelector from "./ColorSelector";
import OptionGroup from "./OptionGroup";

class Actions extends Component {
  constructor(props) {
    super(props);
    this.state = {
      colorVariants: [],
      color: '--',
      band: 'Choose one',
      size: 'Choose one',
      bandOptions: [],
      sizeOptions: [],
      stock: '--',
    };
    this.getColors = this.getColors.bind(this);
    this.handleColorSelected = this.handleColorSelected.bind(this);
    this.handleBandSelected = this.handleBandSelected.bind(this);
    this.handleSizeSelected = this.handleSizeSelected.bind(this);
    this.calculateStock = this.calculateStock.bind(this);
    this.handleAddToBag = this.handleAddToBag.bind(this);
  }

  /*
   * Returns an array with the colors with no repetitions
   */
  getColors() {
    return [...new Set(this.props.variants.map(variant => variant.option1))];
  }

  /*
   * It filters the variants that correspond to the selected color, calculate the stock and
   * sets the state populating those values and the bandOptions needed to populate the BAND OptionGroup
   */
  handleColorSelected(color) {
    const filteredVariants = this.props.variants.filter( variant => variant.option1 === color && variant.inventory_quantity >= 10);
    const stock = this.calculateStock(filteredVariants);

    this.setState({
        colorVariants: filteredVariants,
        color: color,
        band: 'Choose one',
        size: 'Choose one',
        bandOptions: [...new Set(filteredVariants.map(variant => variant.band))],
        sizeOptions: [],
        stock: stock,
    });
  }

  /*
   * It filters the variants that correspond to the selected band size, calculates the stock and
   * sets the state populating those values and the sizeOptions needed to populate the CUP OptionGroup
   */
  handleBandSelected(index) {
    const selectedBand = this.state.bandOptions[index];
    const filteredVariants = this.state.colorVariants.filter( variant => variant.band === selectedBand);
    const stock = this.calculateStock(filteredVariants);
    this.setState({
        band: selectedBand,
        sizeOptions: [...new Set(filteredVariants.map(variant => variant.size))],
        stock: stock,
    });
  }

  /*
   * It filters the variants that correspond to the selected cup size, calculate the stock and sets
   * the state populating those values and the selected cup size
   */
  handleSizeSelected(index) {
    const { colorVariants, band } = this.state;
    const selectedSize = this.state.sizeOptions[index];
    const filteredVariants = colorVariants.filter( variant => variant.band === band && variant.size === selectedSize);
    const stock = this.calculateStock(filteredVariants);

    this.setState({
        size: selectedSize,
        stock: stock,
    });
  }

  /*
   * It receives a list of variants and returns the stock number
   */
  calculateStock(variants) {
    if (!variants.length) {
      return '--';
    }
    const stock = variants.reduce((prev, actual) => {
      return { inventory_quantity: prev.inventory_quantity + actual.inventory_quantity };
    });
    return stock.inventory_quantity;
  }

  /*
   * It alerts a message with the selection details if they were made or if not an advice 
   * to complete the selection 
   */
  handleAddToBag() {
    if (this.state.band === 'Choose one' || this.state.size == 'Choose one') {
      alert('PLEASE SELECT COLOR, BAND SIZE AND CUP SIZE');
    } else {
      alert(`Added a ${this.props.product} - ${this.state.band}${this.state.size} to the cart`);
    }
  }

  render() {
    return ( 
      <div className="actions">
        <div className="actions__options">
            <ColorSelector 
              colors={this.getColors()}
              selectedColor={this.state.color}
              handleColorSelected={this.handleColorSelected}
            />

            <div className="actions__stock">
              STOCK: 
              <span>{this.state.stock}</span>
            </div>

            <div className="actions__size-selection">
              <OptionGroup 
                options={this.state.bandOptions} 
                selectedOption={this.state.band} 
                handleOptionSelected={this.handleBandSelected} 
                label="BAND SIZE"
              />
              <OptionGroup 
                options={this.state.sizeOptions} 
                selectedOption={this.state.size} 
                handleOptionSelected={this.handleSizeSelected} 
                label="CUP SIZE"
              />
            </div>
          </div>
          <div className="actions__submit">
            <button onClick={this.handleAddToBag}>Add to Bag</button>
          </div>
      </div> 
    );
  }
}

export default Actions;
