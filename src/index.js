import React, { Component } from "react";
import ReactDOM from "react-dom";
import axios from "axios";
import Heading from "./components/Heading";
import Gallery from "./components/Gallery";
import Actions from "./components/Actions";
import Description from "./components/Description";
import "./styles/style.scss";

class App extends Component {
  constructor() {
    super();
    this.state = {
      data: { product: { images: [], variants: [ { option1: '', option2: ''}] }},
    };
    this.transformResponse = this.transformResponse.bind(this);
  }
  
  // Fetch product data from external API
  getProduct() {
    let url ="http://www.mocky.io/v2/5bf5b2b0300000df3c7bbf79";
    axios.get(url).then(response => {
      this.setState({
        data: this.transformResponse(response.data),
      });
    });
  }

  componentDidMount() {
    this.getProduct();
  }

  // We modify the variants in order to have BAND and CUP size values separated in 2 keys for future treatment
  transformResponse(productData) {
    productData.product.variants.forEach( variant => {
      const variantAttrs = variant.option2.split(/([0-9]+)/);
      variant.band = variantAttrs[1];
      variant.size = variantAttrs[2];
    });
    return productData;
  }

  render() {
    const { title, images, body_html, variants } = this.state.data.product;
    return (
      <div className="container">
        <Heading title={title} price={variants[0].price}></Heading>
        <div className="container__row">
          <Gallery images={images}></Gallery>
          <Actions variants={variants} product={title}></Actions>
        </div>
        <Description description={body_html}></Description>
      </div>
    );
  }
}

ReactDOM.render(<App />, document.getElementById("root"));
