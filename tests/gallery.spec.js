import React from 'react';
import Gallery from 'Gallery';
import renderer from 'react-test-renderer';

test('Should render the Gallery component', () => {
  const images = [
    {
    "src150": "//cdn.shopify.com/s/files/1/0305/6249/products/Naked1_FullCov_Product_1_05921f37-9d24-4a26-8943-d57f008a2f8a_150x.jpg?v=1528902845",
    "src1130": "//cdn.shopify.com/s/files/1/0305/6249/products/Naked1_FullCov_Product_1_05921f37-9d24-4a26-8943-d57f008a2f8a.jpg?v=1528902845",
    "srcMaster": "//cdn.shopify.com/s/files/1/0305/6249/products/Naked1_FullCov_Product_1_05921f37-9d24-4a26-8943-d57f008a2f8a.jpg?v=1528902845"
    },
    {
    "src150": "//cdn.shopify.com/s/files/1/0916/1906/products/Graystone_LaceRacerback_LaceCheeky_HYB_NY_2017_OCT_004_1440873e-af2a-428c-bf7b-ddc7a4493449_150x.jpg?v=1528760353",
    "src1130": "//cdn.shopify.com/s/files/1/0916/1906/products/Graystone_LaceRacerback_LaceCheeky_HYB_NY_2017_OCT_004_1440873e-af2a-428c-bf7b-ddc7a4493449_1130x.jpg?v=1528760353",
    "srcMaster": "//cdn.shopify.com/s/files/1/0916/1906/products/Graystone_LaceRacerback_LaceCheeky_HYB_NY_2017_OCT_004_1440873e-af2a-428c-bf7b-ddc7a4493449.jpg?v=1528760353"
    }];
  const component = renderer.create(
    <Gallery images={images}></Gallery>
  );
});

