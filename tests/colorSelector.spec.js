import React from 'react';
import ColorSelector from 'ColorSelector';
import renderer from 'react-test-renderer';

test('Should render the ColorSelector component', () => {
  const component = renderer.create(
    <ColorSelector 
    colors={['naked-1','naked-2']}
    selectedColor="naked-1"
    handleColorSelected={function() { return true }}
  />
  );
});

