import React from 'react';
import OptionGroup from 'OptionGroup';
import renderer from 'react-test-renderer';

test('Should render the OptionGroup component', () => {
  const component = renderer.create(
    <OptionGroup 
        options={['32','34']} 
        selectedOption="32" 
        handleOptionSelected={function() { return '32' }} 
        label="BAND SIZE"
        />
  );
});

