import React from 'react';
import Actions from 'Actions';
import ColorSelector from 'ColorSelector';
import OptionGroup from 'OptionGroup';
import enzyme from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

enzyme.configure({ adapter: new Adapter() });

const variants = [
    {
    "id": 6989569458233,
    "price": "68.00",
    "option1": "naked-1",
    "option2": "32E",
    "inventory_quantity": 764,
    "band": "32",
    "size": "E"
    },
    {
    "id": 4615861469221,
    "price": "68.00",
    "option1": "naked-2",
    "option2": "34D",
    "inventory_quantity": 1797,
    "band": "34",
    "size": "D"
    },
    {
    "id": 6989459193913,
    "price": "68.00",
    "option1": "naked-3",
    "option2": "32E",
    "inventory_quantity": 300,
    "band": "32",
    "size": "E"
    },
    {
    "id": 4615725842469,
    "price": "68.00",
    "option1": "naked-4",
    "option2": "32E",
    "inventory_quantity": 214,
    "band": "32",
    "size": "E"
    },
    {
    "id": 6989673398329,
    "price": "68.00",
    "option1": "naked-5",
    "option2": "32E(DD)",
    "inventory_quantity": 98,
    "band": "32",
    "size": "(DD)"
    }
];

describe('Actions component suite', () => {
    it('Should render the component', () => {
        const component = enzyme.mount(<Actions variants={variants} product="Test Product"></Actions>);
    });

    it('Should render a ColorSelector, 2 OptionGroup, the stock and a submit button', () => {
        const component = enzyme.mount(<Actions variants={variants} product="Test Product"></Actions>);
        expect(component.find(ColorSelector)).toHaveLength(1);
        expect(component.find(OptionGroup)).toHaveLength(2);
        expect(component.find('.actions__stock')).toHaveLength(1);
        expect(component.find('.actions__submit')).toHaveLength(1);
    });

    it('Should return an array with the colors', () => {
        const component = enzyme.mount(<Actions variants={variants} product="Test Product"></Actions>);
        const expectedColors = ['naked-1', 'naked-2', 'naked-3', 'naked-4', 'naked-5'];
        const colors = component.instance().getColors();
        expect(colors).toEqual(expectedColors);
    });


    it('Should call calculateStock and populate band OptionGroup with bandSizes for the selected color', () => {
        const component = enzyme.mount(<Actions variants={variants} product="Test Product"></Actions>);
        component.instance().calculateStock = jest.fn();
        component.instance().handleColorSelected('naked-1');
        component.update();

        expect(component.instance().calculateStock.mock.calls).toHaveLength(1);
        expect(component.state().color).toEqual('naked-1');
        expect(component.state().bandOptions).toEqual(['32']);
        expect(component.state().colorVariants).toEqual([{
            "id": 6989569458233,
            "price": "68.00",
            "option1": "naked-1",
            "option2": "32E",
            "inventory_quantity": 764,
            "band": "32",
            "size": "E"
        }]);
        expect(component.find(OptionGroup).at(0).props().options).toEqual(['32']);
    });

    it('Should call calculateStock and populate CUP OptionGroup with sizeOptions for the selected color and band', () => {
        const component = enzyme.mount(<Actions variants={variants} product="Test Product"></Actions>);
        component.instance().calculateStock = jest.fn();
        component.setState({ colorVariants: [{"band": "32", "id": 6989569458233, "inventory_quantity": 764, "option1": "naked-1", "option2": "32E", "price": "68.00", "size": "E"}], 
        bandOptions: ['32'] });
        component.instance().handleBandSelected(0);
        component.update();
    
        expect(component.instance().calculateStock.mock.calls).toHaveLength(1);
        expect(component.state().band).toEqual('32');
        expect(component.state().sizeOptions).toEqual(['E']);
        expect(component.find(OptionGroup).at(0).props().selectedOption).toEqual('32');
        expect(component.find(OptionGroup).at(1).props().options).toEqual(['E']);
    });

    it('Should call calculateStock and set state with the selected cup size', () => {
        const component = enzyme.mount(<Actions variants={variants} product="Test Product"></Actions>);
        component.instance().calculateStock = jest.fn();
        component.setState({ colorVariants: [{"band": "32", "id": 6989569458233, "inventory_quantity": 764, "option1": "naked-1", "option2": "32E", "price": "68.00", "size": "E"}], 
        bandOptions: ['32'], 'sizeOptions': ['E'] });
        component.instance().handleSizeSelected(0);
        component.update();

        expect(component.instance().calculateStock.mock.calls).toHaveLength(1);
        expect(component.state().size).toEqual('E');
        expect(component.find(OptionGroup).at(1).props().selectedOption).toEqual('E');
    });

    it('Should return the stock for given variants', () => {
        const component = enzyme.mount(<Actions variants={variants} product="Test Product"></Actions>);
        const variant = [{"band": "32", "id": 6989569458233, "inventory_quantity": 764, "option1": "naked-1", "option2": "32E", "price": "68.00", "size": "E"}];
        const stock = component.instance().calculateStock(variant);
        expect(stock).toBe(764);
    });
});
