import React from 'react';
import Description from 'Description';
import renderer from 'react-test-renderer';

test('Should render the Description component', () => {
  const body_html = "<meta charset=\"utf-8\">\n<p>A slightly fuller coverage version of the bra that started it all. The 24/7™ Classic Full Coverage Bra’s signature cups are designed with hybrid memory foam that does double duty, providing softness inside and support outside. It forms to your unique shape and creates a smooth silhouette, no matter what you’re wearing. The straps are lined with supple memory foam so they stay put and never dig in, and the ballet back design ensures your straps will truly never slip. You’ll wonder how you ever lived without it.</p>\n<!-- split -->\n<ul>\n<li><span>Available in B-H, 32-46 bands</span></li>\n<li><span>Ultra-thin hybrid foam, has memory foam on the inside to form to your body, and a supportive foam on the outside</span></li>\n<li><span>Incredibly soft micro jersey fabric, knitted for maximum durability</span></li>\n<li><span>Double layer ballet back with hidden elastic smooths your silhouette</span></li>\n<li><span>Memory foam straps that won't slip </span></li>\n<li>Pleated detail at the center front</li>\n<li><span>Gold alloy hardware adds a touch of shine</span></li>\n<li><span>Foam-padded hook &amp; eye with tagless (scratch-free!) printed label</span></li>\n<li><span>Flexible nylon-coated nickel-free wires</span></li>\n<li><span>Nylon/spandex</span></li>\n</ul>";
  const component = renderer.create(
    <Description description={body_html}></Description>
  );
});

